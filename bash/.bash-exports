export PATH=/usr/lib64/ccache:$PATH

#export GIT_AUTHOR_EMAIL=amit@kernel.org
#export GIT_COMMITTER_EMAIL=${GIT_AUTHOR_EMAIL}

# Have not linked this in /etc/profile.d/
#source /usr/share/git-core/contrib/completion/git-prompt.sh

export GIT_PS1_SHOWDIRTYSTATE=true
export GIT_PS1_SHOWUNTRACKEDFILES=true

#export PS1='\[\033[00;36m\]\u@\h\[\033[00m\]:\[\033[01;34m\] \w\[\033[00m\]$(__git_ps1 " (%s)")\$ '
#export PS1='\[\033[01;34m\][\t]\[\033[00m\] \[\033[00;36m\]\u@\h\[\033[00m\]:\[\033[01;34m\] \w\[\033[00m\]$(__git_ps1 " (%s)")\$ '
export PS1='\[\033[01;34m\][\t]\[\033[00m\] \[\033[00;36m\]\u@\h\[\033[00m\]:\[\033[01;34m\] \w\[\033[00m\]\$ '

alias ec=emacsclient

# http://udrepper.livejournal.com/11429.html
# https://lists.fedoraproject.org/pipermail/devel/2010-May/135667.html
export MALLOC_PERTURB_=$(($RANDOM % 255 + 1))

# From Jim Meyering
gpg() { gpg2 "$@"; }

# From http://blog.tuxforge.com/bash-history-duplicates/
# From
# http://stackoverflow.com/questions/338285/prevent-duplicates-from-being-saved-in-bash-history
export HISTCONTROL=erasedups:ignoredups 
shopt -s histappend                       # append history file
export PROMPT_COMMAND="history -a"        # update histfile after every command
export HISTIGNORE='&:ls:cd:cd ..:[bf]g:exit:h:history'

# qemu will create tmp files in this location -- useful for snapshots
export TMPDIR=/var/tmp

# https://plus.google.com/+KonstantinRyabitsev/posts/1uzxoBc1vxY
alias engage="play -V1 -c2 -n synth whitenoise band -n 100 24 band -n 300 100 gain +20"

# too many crashes :(
#alias mutt="screen mutt"
#alias offlineimap="screen offlineimap"

alias mutt="cd /var/tmp/; mutt"

# From f-devel
# From Xose Vazquez Perez
ilm() # info like man
{
 info $1 --subnodes --output - 2>/dev/null | less
}

# man gpg-agent
GPG_TTY=$(tty)
export GPG_TTY

# https://github.com/lfit/ssh-gpg-smartcard-config/blob/master/YubiKey_NEO.rst
if [ ! -f /run/user/$(id -u)/gpg-agent.env ]; then
    killall gpg-agent;
    eval $(gpg-agent --daemon --enable-ssh-support > /run/user/$(id -u)/gpg-agent.env);
fi
. /run/user/$(id -u)/gpg-agent.env

#https://spin.atomicobject.com/2016/05/28/log-bash-history/
export PROMPT_COMMAND='if [ "$(id -u)" -ne 0 ]; then echo "$(date "+%Y-%m-%d.%H:%M:%S") $(pwd) $(history 1)" >> ~/.logs/bash-history-$(date "+%Y-%m-%d").log; fi'
