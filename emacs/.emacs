;; .emacs

;;; uncomment this line to disable loading of "default.el" at startup
(setq inhibit-default-init t)

;; Don't insert instructions in the *scratch* buffer
(setq initial-scratch-message nil)

;; enable visual feedback on selections
(setq transient-mark-mode t)

;; default to better frame titles
(setq frame-title-format
      (concat  "%b - emacs@" (system-name)))

;; default to unified diffs
(setq diff-switches "-u")

;; always end a file with a newline
(setq require-final-newline t)

;;; uncomment for CJK utf-8 support for non-Asian users
;; (require 'un-define)

;;; my customisations


(setq default-frame-alist
      '((wait-for-wm . nil)
        (top . 0)
        (width . 81)
	(length . 54)
        (tool-bar-lines . 0)
        (menu-bar-lines . 0)))

(global-font-lock-mode t)

(setq font-use-system-font t)

(cond (window-system
       (mwheel-install)
))

(require 'color-theme)
(color-theme-initialize)
(color-theme-dark-laptop)

(add-hook 'text-mode-hook
       '(lambda () (auto-fill-mode 1)))

; turn-on-auto-fill is shortcut for above.
(defun my-message-mode-hook ()
  (auto-fill-mode 1)
  (turn-on-filladapt-mode))
(add-hook 'message-mode-hook 'my-message-mode-hook)

;(if window-system
;    (color-theme-dark-laptop))
;  (progn
;    (color-theme-gray30)
;    (auto-fill-mode)))

;;;(add-hook 'message-mode-hook
;;;          '(lambda () (if (string-match "mutt-" (buffer-file-name))
;;;                          (message-mode))))

(global-set-key (kbd "C-c f") 'auto-fill-mode)

;; Stop at the end of the file, not just add lines
(setq next-line-add-newlines nil)

(defun linux-c-mode ()
  "C mode with adjusted defaults for use with the Linux kernel."
  (interactive)
  (c-mode)
  (c-set-style "K&R")
  (setq auto-fill-mode nil)
  (setq c-basic-offset 8))

(defun qemu-c-mode ()
  "C mode with adjusted defaults for use with qemu"
  (interactive)
  (c-mode)
  (c-set-style "bsd")
  (setq indent-tabs-mode nil)
  (setq auto-fill-mode nil)
  (setq c-basic-offset 4))

;;; When editing C sources in libvirt, use this style.
(defun libvirt-c-mode ()
  "C mode with adjusted defaults for use with libvirt."
  (interactive)
  (c-set-style "K&R")
  (setq indent-tabs-mode nil) ; indent using spaces, not TABs
  (setq c-indent-level 4)
  (setq auto-fill-mode nil)
  (setq c-basic-offset 4))

(add-hook 'c-mode-hook
          '(lambda () (if (string-match "/libvirt" (buffer-file-name))
                          (libvirt-c-mode))))

(define-abbrev-table 'global-abbrev-table '(
  ("8ack" "Acked-by: Amit Shah <amit@kernel.org>" nil 0)
  ("8rev" "Reviewed-by: Amit Shah <amit@kernel.org>" nil 0)
  ("8sig" "Signed-off-by: Amit Shah <amit@kernel.org>" nil 0)
  )
)

;;; stop asking whether to save newly added abbrev when quitting emacs
(setq save-abbrevs nil)

(abbrev-mode 1)

;;;(setq auto-mode-alist (cons '("*.[ch]$" . linux-c-mode)
;;;                       auto-mode-alist))

;;;(require 'paren)
;;;(show-paren-mode t)

;;;(require 'notmuch)

;;;(blink-cursor-mode (- (*) (*) (*)))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(blink-cursor-mode nil)
 '(inhibit-startup-screen t)
 '(show-paren-mode t)
 '(speedbar-default-position (quote left))
 '(text-mode-hook (quote (turn-on-auto-fill (lambda nil (auto-fill-mode 1)) text-mode-hook-identify)))
 '(word-wrap t))
(put 'narrow-to-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
